/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package westsidenibbasgoogol;

import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Mohd Aiman
 */
public class VSAI extends javax.swing.JFrame {
    
    
    int turn =1;
    
    int buttonUsed[] = {0,0,0,0,0,0,0,0,0};
    
    int playerwon[] = {0,0,0,0,0,0,0,0,0};
    
    int compwon[] = {0,0,0,0,0,0,0,0,0};
    
    int player_result,comp_result;
    
    Random r = new Random();
    
    //check win for player
    int player_Win()
    {
        if (playerwon[0] == 1 && playerwon[1] == 1 && playerwon[2] == 1 )
        {
            return 1;
        }
        if (playerwon[3] == 1 && playerwon[4] == 1 && playerwon[5] == 1 )
        {
            return 1;
        }
        if (playerwon[6] == 1 && playerwon[7] == 1 && playerwon[8] == 1 )
        {
            return 1;
        }
        if (playerwon[0] == 1 && playerwon[3] == 1 && playerwon[6] == 1 )
        {
            return 1;
        }
        if (playerwon[1] == 1 && playerwon[4] == 1 && playerwon[7] == 1 )
        {
            return 1;
        }
        if (playerwon[2] == 1 && playerwon[5] == 1 && playerwon[8] == 1 )
        {
            return 1;
        }
        if (playerwon[0] == 1 && playerwon[4] == 1 && playerwon[8] == 1 )
        {
            return 1;
        }
        if (playerwon[2] == 1 && playerwon[4] == 1 && playerwon[6] == 1 )
        {
            return 1;
        }
        
        else if(buttonUsed[0] == 1 && buttonUsed[1] == 1 &&buttonUsed[2] == 1 && buttonUsed[3] == 1 &&buttonUsed[4] == 1 && buttonUsed[5] == 1 &&buttonUsed[6] == 1 && buttonUsed[7] == 1 && buttonUsed[8] == 1 )
        {
            return 2;
        }
        
        return 0;
                
    }
    
    //CHECK IF COMP CAN WIN
    int comp_Win()
    {
        if (compwon[0] == 1 && compwon[1] == 1 && compwon[2] == 1 )
        {
            return 1;
        }
        if (compwon[3] == 1 && compwon[4] == 1 && compwon[5] == 1 )
        {
            return 1;
        }
        if (compwon[6] == 1 && compwon[7] == 1 && compwon[8] == 1 )
        {
            return 1;
        }
        if (compwon[0] == 1 && compwon[3] == 1 && compwon[6] == 1 )
        {
            return 1;
        }
        if (compwon[1] == 1 && compwon[4] == 1 && compwon[7] == 1 )
        {
            return 1;
        }
        if (compwon[2] == 1 && compwon[5] == 1 && compwon[8] == 1 )
        {
            return 1;
        }
        if (compwon[0] == 1 && compwon[4] == 1 && compwon[8] == 1 )
        {
            return 1;
        }
        if (compwon[2] == 1 && compwon[4] == 1 && compwon[6] == 1 )
        {
            return 1;
        }
        else if(buttonUsed[0] == 1 && buttonUsed[1] == 1 &&buttonUsed[2] == 1 && buttonUsed[3] == 1 &&buttonUsed[4] == 1 && buttonUsed[5] == 1 &&buttonUsed[6] == 1 && buttonUsed[7] == 1 && buttonUsed[8] == 1 )
        {
            return 2;
        }
        return 0;
                
    }
    
    //COMPUTER'S MOVE
    public void computer(java.awt.event.ActionEvent evt)
    {
        if(compwon[0]==1 && compwon[1]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
        
        }
        
         else if(compwon[0]==1 && compwon[2]==1 && buttonUsed[1]==0)
        {
            
            bt2ActionPerformed(evt);
            
            
        }
        else if(compwon[1]==1 && compwon[2]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
        
        else if(compwon[3]==1 && compwon[4]==1 && buttonUsed[5]==0)
        {
            
            bt6ActionPerformed(evt);
                       
        }
        
        else if(compwon[3]==1 && compwon[5]==1 && buttonUsed[4]==0)
        {
          
            bt5ActionPerformed(evt);
            
            
        }
        else if(compwon[4]==1 && compwon[5]==1 && buttonUsed[3]==0)
        {
            
            bt4ActionPerformed(evt);
           
        }
            
        else if(compwon[6]==1 && compwon[7]==1 && buttonUsed[8]==0)
        {
           
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(compwon[6]==1 && compwon[8]==1 && buttonUsed[7]==0)
        {
            
            bt8ActionPerformed(evt);
            
        }
            
        else if(compwon[7]==1 && compwon[8]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
            
        else if(compwon[0]==1 && compwon[3]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
           
        else if(compwon[0]==1 && compwon[6]==1 && buttonUsed[3]==0)
        {

            bt4ActionPerformed(evt);
            
        }
            
        else if(compwon[3]==1 && compwon[6]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
            
        else if(compwon[1]==1 && compwon[4]==1 && buttonUsed[7]==0)
        {
            
            bt8ActionPerformed(evt);
            
        }
            
        else if(compwon[1]==1 && compwon[7]==1 && buttonUsed[4]==0)
        {
           
            bt5ActionPerformed(evt);
            
        }
        
        else if(compwon[4]==1 && compwon[7]==1 && buttonUsed[1]==0)
        {
            
            bt2ActionPerformed(evt);
            
        }
        
        else if(compwon[2]==1 && compwon[5]==1 && buttonUsed[8]==0)
        {
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(compwon[2]==1 && compwon[8]==1 && buttonUsed[5]==0)
        {
            
            bt6ActionPerformed(evt);
            
        }
            
        else if(compwon[5]==1 && compwon[8]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
        }
            
        else if(compwon[0]==1 && compwon[4]==1 && buttonUsed[8]==0)
        {
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(compwon[0]==1 && compwon[8]==1 && buttonUsed[4]==0)
        {
            
            bt5ActionPerformed(evt);
            
        }
            
        else if(compwon[4]==1 && compwon[8]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
            
        else if(compwon[2]==1 && compwon[4]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
            
        else if(compwon[2]==1 && compwon[6]==1 && buttonUsed[4]==0)
        {
            
            bt5ActionPerformed(evt);
            
        }
            
        else if(compwon[4]==1 && compwon[6]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
            
        }
        
        
        
        else if(playerwon[0]==1 && playerwon[1]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
        }
            
       else if(playerwon[0]==1 && playerwon[2]==1 && buttonUsed[1]==0)
        {
            
            bt2ActionPerformed(evt);
            
            
        }
        else if(playerwon[1]==1 && playerwon[2]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
        
        else if(playerwon[3]==1 && playerwon[4]==1 && buttonUsed[5]==0)
        {
            
            bt6ActionPerformed(evt);
                       
        }
        
        else if(playerwon[3]==1 && playerwon[5]==1 && buttonUsed[4]==0)
        {
          
            bt5ActionPerformed(evt);
            
            
        }
        else if(playerwon[4]==1 && playerwon[5]==1 && buttonUsed[3]==0)
        {
            
            bt4ActionPerformed(evt);
           
        }
            
        else if(playerwon[6]==1 && playerwon[7]==1 && buttonUsed[8]==0)
        {
           
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(playerwon[6]==1 && playerwon[8]==1 && buttonUsed[7]==0)
        {
            
            bt8ActionPerformed(evt);
            
        }
            
        else if(playerwon[7]==1 && playerwon[8]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
            
        else if(playerwon[0]==1 && playerwon[3]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
           
        else if(playerwon[0]==1 && playerwon[6]==1 && buttonUsed[3]==0)
        {

            bt4ActionPerformed(evt);
            
        }
            
        else if(playerwon[3]==1 && playerwon[6]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
            
        else if(playerwon[1]==1 && playerwon[4]==1 && buttonUsed[7]==0)
        {
            
            bt8ActionPerformed(evt);
            
        }
            
        else if(playerwon[1]==1 && playerwon[7]==1 && buttonUsed[4]==0)
        {
           
            bt5ActionPerformed(evt);
            
        }
        
        else if(playerwon[4]==1 && playerwon[7]==1 && buttonUsed[1]==0)
        {
            
            bt2ActionPerformed(evt);
            
        }
        
        else if(playerwon[2]==1 && playerwon[5]==1 && buttonUsed[8]==0)
        {
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(playerwon[2]==1 && playerwon[8]==1 && buttonUsed[5]==0)
        {
            
            bt6ActionPerformed(evt);
            
        }
            
        else if(playerwon[5]==1 && playerwon[8]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
        }
            
        else if(playerwon[0]==1 && playerwon[4]==1 && buttonUsed[8]==0)
        {
            
            bt9ActionPerformed(evt);
            
        }
            
        else if(playerwon[0]==1 && playerwon[8]==1 && buttonUsed[4]==0)
        {
            
            bt5ActionPerformed(evt);
            
        }
            
        else if(playerwon[4]==1 && playerwon[8]==1 && buttonUsed[0]==0)
        {
            
            bt1ActionPerformed(evt);
            
        }
            
        else if(playerwon[2]==1 && playerwon[4]==1 && buttonUsed[6]==0)
        {
            
            bt7ActionPerformed(evt);
            
        }
            
        else if(playerwon[2]==1 && playerwon[6]==1 && buttonUsed[4]==0)
        {
            
            bt5ActionPerformed(evt);
            
        }
            
        else if(playerwon[4]==1 && playerwon[6]==1 && buttonUsed[2]==0)
        {
            
            bt3ActionPerformed(evt);
            
            
        }
        
        
        else
            random(evt);
        
        
        
            
        
    }
    
    
    public void random(java.awt.event.ActionEvent evt)//random move for computer
    {
        int n;
        while(true)
        {
            n = r.nextInt(9);
            
            if(n==8 && buttonUsed[8]==0)
            {
                
                bt9ActionPerformed(evt);
                break;
            }
            else if(n == 7 && buttonUsed[7]==0)
            {
                bt8ActionPerformed(evt);
                break;
            }
            else if(n == 6 && buttonUsed[6]==0)
            {
                bt7ActionPerformed(evt);
                break;
            }
            else if(n == 5 && buttonUsed[5]==0)
            {
                bt6ActionPerformed(evt);
                break;
            }
            else if(n == 4 && buttonUsed[4]==0)
            {
                bt5ActionPerformed(evt);
                break;
            }
            else if(n == 3 && buttonUsed[3]==0)
            {
                bt4ActionPerformed(evt);
                break;
            }
            else if(n == 2 && buttonUsed[2]==0)
            {
                bt3ActionPerformed(evt);
                break;
            }
            else if(n == 1 && buttonUsed[1]==0)
            {
                bt2ActionPerformed(evt);
                break;
            }
            else if(n == 0 && buttonUsed[0]==0)
            {
                bt1ActionPerformed(evt);
                break;
            }
            
           
           
        }
            
               
        
            
        
    }

    /**
     * Creates new form TTT
     */
    public VSAI() {
        initComponents();
        
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bt6 = new javax.swing.JButton();
        reset = new javax.swing.JButton();
        bt7 = new javax.swing.JButton();
        bt8 = new javax.swing.JButton();
        bt4 = new javax.swing.JButton();
        bt5 = new javax.swing.JButton();
        bt1 = new javax.swing.JButton();
        bt2 = new javax.swing.JButton();
        bt3 = new javax.swing.JButton();
        turn2 = new javax.swing.JButton();
        turn1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        bt9 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        bt6.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt6ActionPerformed(evt);
            }
        });

        reset.setText("RESET GAME");
        reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetActionPerformed(evt);
            }
        });

        bt7.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt7ActionPerformed(evt);
            }
        });

        bt8.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt8ActionPerformed(evt);
            }
        });

        bt4.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt4ActionPerformed(evt);
            }
        });

        bt5.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt5ActionPerformed(evt);
            }
        });

        bt1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt1ActionPerformed(evt);
            }
        });

        bt2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt2ActionPerformed(evt);
            }
        });

        bt3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt3ActionPerformed(evt);
            }
        });

        turn2.setText("FIRST");
        turn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                turn2ActionPerformed(evt);
            }
        });

        turn1.setText("SECOND");
        turn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                turn1ActionPerformed(evt);
            }
        });

        jLabel1.setText("SELECT TURN:");

        jButton1.setText("BACK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        bt9.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        bt9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(305, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(134, 134, 134))))
            .addGroup(layout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(turn2)
                .addGap(18, 18, 18)
                .addComponent(turn1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(73, 73, 73)
                            .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addContainerGap(14, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(345, Short.MAX_VALUE)
                .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(turn2)
                    .addComponent(turn1))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(63, 63, 63)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(reset, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(53, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt6ActionPerformed
        // TODO add your handling code here:

        if (buttonUsed[5] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt6.setText("X");
                
                buttonUsed[5]=1;
                
                playerwon[5]=1;
                
                player_result = player_Win();
                
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                    default:
                        computer(evt);
                        break;
                }
                
                
            }
            
             else if(turn == 2)
            {
                turn = 1;
                bt6.setText("O");
                buttonUsed[5]=1;
                
                compwon[5]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt6ActionPerformed

    private void resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetActionPerformed
        // TODO add your handling code here:

        bt1.setText("");
        bt2.setText("");
        bt3.setText("");
        bt4.setText("");
        bt5.setText("");
        bt6.setText("");
        bt7.setText("");
        bt8.setText("");
        bt9.setText("");
        
        turn = 1;
        
        for (int i = 0; i < 9; i++) 
        {
             
            playerwon[i]=0;
            
        }
        for (int i = 0; i < 9; i++) 
        {
             
            compwon[i]=0;
            
        }
        for (int i = 0; i < 9; i++) 
        {
             
            buttonUsed[i]=0;
            
        }
    }//GEN-LAST:event_resetActionPerformed

    private void bt7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt7ActionPerformed
        // TODO add your handling code here:

          if (buttonUsed[6] == 0)
        {
            
            if(turn == 1)
            {
                turn = 2;
                bt7.setText("X");
                
                buttonUsed[6]=1;
                 
                playerwon[6]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break ;
  
                    // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                 
                    //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;
                }
                
                
            }
            
            else if(turn == 2)
            {
                turn = 1;
                bt7.setText("O");
                buttonUsed[6]=1;
                compwon[6]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
             // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt7ActionPerformed

    private void bt8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt8ActionPerformed
        // TODO add your handling code here:

         if (buttonUsed[7] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt8.setText("X");
                
                 buttonUsed[7]=1;
                 
                playerwon[7]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                        
                    // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                
                    //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;   
                }
                
                
            }
            
             else if(turn == 2)
            {
                turn = 1;
                bt8.setText("O");
                buttonUsed[7]=1;
                compwon[7]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
         // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt8ActionPerformed

    private void bt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt4ActionPerformed
        // TODO add your handling code here:

        if (buttonUsed[3] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt4.setText("X");
                
                buttonUsed[3]=1;
                playerwon[3]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                        
                // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                        
                //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;
                }
            }
            
            else if(turn == 2)
            {
                turn = 1;
                bt4.setText("O");
                buttonUsed[3]=1;
                
                compwon[3]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
            
        }

        // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt4ActionPerformed

    private void bt5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt5ActionPerformed
        // TODO add your handling code here:

          if (buttonUsed[4] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt5.setText("X");
                
                buttonUsed[4]=1;
                
                playerwon[4]=1;
                
                player_result = player_Win();
                
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                    default:
                        computer(evt);
                        break;
                }
            }
            
            
             else if(turn == 2)
            {
                turn = 1;
                bt5.setText("O");
                buttonUsed[4]=1;
                compwon[4]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt5ActionPerformed

    private void bt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt1ActionPerformed
        // TODO add your handling code here:
        if (buttonUsed[0] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt1.setText("X");
                
                buttonUsed[0]=1;
                 
                playerwon[0]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                        
                // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                        
                //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;
                }
                
                
            }
            
             else if(turn == 2)
            {
                turn = 1;
                bt1.setText("O");
                buttonUsed[0]=1;
                
                compwon[0]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
         // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }


    }//GEN-LAST:event_bt1ActionPerformed

    private void bt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt2ActionPerformed
        // TODO add your handling code here:

        if (buttonUsed[1] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt2.setText("X");
                buttonUsed[1]=1;
                
                playerwon[1]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                        
                // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                        
                //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;
                }
            }
            
             else if(turn == 2)
            {
                turn = 1;
                bt2.setText("O");
                buttonUsed[1]=1;
                compwon[1]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
        // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }
    }//GEN-LAST:event_bt2ActionPerformed

    private void bt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt3ActionPerformed
        // TODO add your handling code here:

        if (buttonUsed[2] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt3.setText("X");
                buttonUsed[2]=1;
                playerwon[2]=1;
                
                
                player_result = player_Win();
                
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                    case 2:// IF FULL BUT NOBODY WIN
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                    default://COMPUTER'S TURN
                        computer(evt);
                        break;     
                }
                 

                 // IF FULL BUT NOBODY WIN
                 //COMPUTER'S TURN
                 // IF TEXT HAS BEEN SET AT THIS BUTTON
            }
            
            
            else if(turn == 2)
            {
                turn = 1;
                bt3.setText("O");
                buttonUsed[2]=1;
                compwon[2]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                    
                }
                else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
                
                
            
            }
        }
        // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt3ActionPerformed

    private void turn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_turn2ActionPerformed
        // TODO add your handling code here:

        turn = 1;
        
       if(buttonUsed[0] == 1 || buttonUsed[1] == 1 || buttonUsed[2] == 1 || buttonUsed[3] == 1 || buttonUsed[4] == 1 || buttonUsed[5] == 1 || buttonUsed[6] == 1 || buttonUsed[7] == 1 || buttonUsed[8] == 1 )
        {
            int response = JOptionPane.showConfirmDialog(this, "DO YOU EXIT CURRENT GAME?", "TIC TAC TOE",JOptionPane.YES_NO_OPTION);
            
            if(response==JOptionPane.YES_OPTION)
        {
            resetActionPerformed(evt);
        }
        }

    }//GEN-LAST:event_turn2ActionPerformed

    private void turn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_turn1ActionPerformed
        // TODO add your handling code here:

        if(buttonUsed[0] == 1 || buttonUsed[1] == 1 || buttonUsed[2] == 1 || buttonUsed[3] == 1 || buttonUsed[4] == 1 || buttonUsed[5] == 1 || buttonUsed[6] == 1 || buttonUsed[7] == 1 || buttonUsed[8] == 1 )
        {
            int response = JOptionPane.showConfirmDialog(this, "DO YOU EXIT CURRENT GAME?", "TIC TAC TOE",JOptionPane.YES_NO_OPTION);
            
            if(response==JOptionPane.YES_OPTION)
            {
                resetActionPerformed(evt);
                turn = 2;
         
                computer(evt);
            }
            
            
        }
        
        if(buttonUsed[0] == 0 && buttonUsed[1] == 0 &&buttonUsed[2] == 0 && buttonUsed[3] == 0 &&buttonUsed[4] == 0 && buttonUsed[5] == 0 &&buttonUsed[6] == 0 && buttonUsed[7] == 0 && buttonUsed[8] == 0)
        {
            turn = 2;
         
            computer(evt);
            
        }
         

    }//GEN-LAST:event_turn1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        this.toBack();
        this.dispose();
        TTTMain menu = new TTTMain();
        menu.setVisible(true);
        menu.setState(java.awt.Frame.NORMAL);
        menu.toFront();

    }//GEN-LAST:event_jButton1ActionPerformed

    private void bt9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt9ActionPerformed
        // TODO add your handling code here:

         if (buttonUsed[8] == 0)
        {
            if(turn == 1)
            {
                turn = 2;
                bt9.setText("X");
                
                buttonUsed[8]=1;
                
                playerwon[8]=1;
                
                player_result = player_Win();
                switch (player_result) {
                    case 1:
                        JOptionPane.showMessageDialog(rootPane, "** Player Win!! **");
                        turn = 3;
                        break;
                        
                // IF FULL BUT NOBODY WIN
                    case 2:
                        JOptionPane.showMessageDialog(rootPane, "DRAW");
                        break;
                        
                //COMPUTER'S TURN
                    default:
                        computer(evt);
                        break;
                }
                
            }
            
             else if(turn == 2)
            {
                turn = 1;
                bt9.setText("O");
                buttonUsed[8]=1;
                compwon[8]=1;
                
                comp_result = comp_Win();
                
                if(comp_result == 1)
                {
                    JOptionPane.showMessageDialog(rootPane, "** Computer Win!! **");
                    turn = 3;
                }
                
                 else if(comp_result == 2)
                {
                    JOptionPane.showMessageDialog(rootPane, "DRAW");
                }
            
            }
        }
        
        // IF TEXT HAS BEEN SET AT THIS BUTTON
        else
        {
            JOptionPane.showMessageDialog(rootPane, " ALREADY ASSIGNED");
        }

    }//GEN-LAST:event_bt9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VSAI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VSAI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VSAI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VSAI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VSAI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt1;
    private javax.swing.JButton bt2;
    private javax.swing.JButton bt3;
    private javax.swing.JButton bt4;
    private javax.swing.JButton bt5;
    private javax.swing.JButton bt6;
    private javax.swing.JButton bt7;
    private javax.swing.JButton bt8;
    private javax.swing.JButton bt9;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton reset;
    private javax.swing.JButton turn1;
    private javax.swing.JButton turn2;
    // End of variables declaration//GEN-END:variables
}
