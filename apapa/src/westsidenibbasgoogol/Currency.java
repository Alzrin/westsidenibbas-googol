/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package westsidenibbasgoogol;

/**
 *
 * @author Hp
 */

public class Currency {
    String name;
    double currency;
    
    //normal constructor
    public Currency(String name, double currency)
    {
        this.name = name;
        this.currency = currency;
    }
    
    //accessor
    public String getName(){ return name; }
    public double getCurrency(){ return currency; }
    
    //mutator
    public void setName(String name)
    {
        
    }
    
    //toString
    public String toString()
    {
        return("\nNAME: " + name + "\nCURRENCY: " + currency);
    }
}