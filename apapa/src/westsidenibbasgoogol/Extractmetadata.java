package westsidenibbasgoogol;

import java.util.Scanner;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class Extractmetadata {

    public static void main(String[] args) throws IOException {

        try {
            PrintWriter output = new PrintWriter(new FileOutputStream("metadata.txt"));

            Document document;
            int i = 0;
            String[] reference = new String[500];

            try {
                Scanner fibba = new Scanner(new FileInputStream("top-500.txt"));
                while (fibba.hasNextLine()) {
                    String line = fibba.nextLine();
                    String[] details = line.split(",");
                    String web = details[1];
                    reference[i] = web;
                    i++;
                }
            } catch (IOException e) {
            }

            i = 0;
            for (int j = 0; j < 500; j++) {
                try {
                    if (i < 500) {
                        document = Jsoup.connect("https://www." + reference[i]).get();

                        try {
                            String description = document.select("meta[name=description]").get(0).attr("content");

                            output.print(reference[i] + ":" + description);
                            System.out.println(reference[i] + ":" + description);

                        } catch (Exception e) {
                            output.print(reference[i] + ": ");
                            System.out.print(reference[i] + ": ");
                            
                        }

                        try {
                            String keywords = document.select("meta[name=keywords]").first().attr("content");
                            //Print keywords.
                            output.print(" " + keywords + "\n");
                            System.out.println(" " + keywords + "\n");

                        } catch (Exception e) {
                            output.print(": \n");
                            System.out.println(": \n");
                        }
                        i++;

                    }

                } catch (IOException e) {
                    output.print(reference[i] +": can't access\n");
                    System.out.println(reference[i] +": can't access\n");
                    j = i;
                    i++;
                }
            }

            output.close();
        } catch (IOException e) {
            System.out.println("problem with file output");
        }

    }
}
